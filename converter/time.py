from enum import Enum
import math


_leaps = [
    46828800, 78364801, 109900802,
    173059203, 252028804, 315187205,
    346723206, 393984007, 425520008,
    457056009, 504489610, 551750411,
    599184012, 820108813, 914803214,
    1025136015, 1119744016, 1167264017
    ]


EPOCHS_TICKS_DIFFERENCE = 315964800


class ConversionMode(Enum):
    UNIX2GPS = 1
    GPS2UNIX = 2


def _is_leap(gps_time: str) -> bool:
    """
    Test to see if a GPS second is a leap second
    """
    return int(float(gps_time)) in _leaps


def _count_leaps(gps_time: float, conversion_mode: ConversionMode) -> int:
    """
    Count number of leap seconds that have passed
    """
    leaps_count = 0
    for i, leap in enumerate(_leaps):
        # GPS time labels each second uniquely including leap seconds while Unix time does not,
        # preferring to count a constant number of seconds a day including those containing leap seconds.
        if conversion_mode is ConversionMode.UNIX2GPS and gps_time >= (leap - i):
            leaps_count += 1
            continue
        if conversion_mode is ConversionMode.GPS2UNIX and gps_time >= leap:
            leaps_count += 1
            continue

    return leaps_count


def unix2gps(unix_time: str):
    """
    Convert Unix Time to GPS Time.

    https://www.andrews.edu/~tzs/timeconv/timealgorithm.html
    """
    if math.fmod(unix_time, 1) != 0:
        unix_time -= 0.5
        is_leap = True
    else:
        is_leap = False
    gps_time = unix_time - 315964800
    n_leaps = _count_leaps(gps_time, ConversionMode.UNIX2GPS)
    gps_time = gps_time + n_leaps + int(is_leap)
    return gps_time


def gps2unix(gps_time: str) -> int:
    """
    Convert GPS Time to Unix Time

    https://www.andrews.edu/~tzs/timeconv/timealgorithm.html
    """
    time = float(gps_time)
    leaps_count = _count_leaps(time, ConversionMode.GPS2UNIX)

    unix_time = time + EPOCHS_TICKS_DIFFERENCE - leaps_count

    # leap seconds the Unix time is given an additional .5 seconds from
    # the previous second to distinguish it from the seconds immediately before and afterwards
    if _is_leap(gps_time):
        unix_time += 0.5

    return int(unix_time)