import pandas as pd
from converter.time import unix2gps


ORIGINAL_GPS = 'GPSTime(sec)'
ORIGINAL_X = 'Easting(m)'
ORIGINAL_Y = 'Northing(m)'
ORIGINAL_Z = 'Height(m)'
ORIGINAL_LAT = 'Latitude(deg)'
ORIGINAL_LON = 'Longitude(deg)'
ORIGINAL_ROLL = 'Roll(deg)'
ORIGINAL_PITCH = 'Pitch(deg)'
ORIGINAL_HEADING = 'Heading(deg)'


GPS = 'gps_seconds[s]'
FILENAME = 'file_name'
LAT = 'latitude[deg]'
LON = 'longitude[deg]'
ALT = 'altitude_ellipsoidal[m]'
ROLL = 'roll[deg]'
PITCH = 'pitch[deg]'
HEADING = 'heading[deg]'
X = 'projectedX[m]'
Y = 'projectedY[m]'
Z = 'projectedZ[m]'


def map_reference(original_path: str, save_path: str) -> pd.DataFrame:
    """Convert original .PosC panoramas reference to inner images meta .csv file

    Args:
        original_path (str): Path of the original .PosC panoramas reference

        save_path (str): Path to save inner images meta .csv file

    Returns:
        pd.DataFrame: DataFrame corresponding to resulting meta
    """
    original = pd.read_csv(original_path, sep=' ')

    reference = pd.DataFrame(columns=[GPS, FILENAME, LAT, LON, ALT, ROLL, PITCH, HEADING, X, Y, Z])
    filenames = pd.DataFrame([f'{i:06d}' for i in range(len(original))])[0]

    reference[GPS] = original[ORIGINAL_GPS].apply(unix2gps)
    reference[FILENAME] = filenames
    reference[LAT] = original[ORIGINAL_LAT]
    reference[LON] = original[ORIGINAL_LON]
    reference[ALT] = original[ORIGINAL_Z]
    reference[ROLL] = original[ORIGINAL_ROLL]
    reference[PITCH] = original[ORIGINAL_PITCH]
    reference[HEADING] = original[ORIGINAL_HEADING]
    reference[X] = original[ORIGINAL_X]
    reference[Y] = original[ORIGINAL_Y]
    reference[Z] = original[ORIGINAL_Z]

    reference.to_csv(save_path, sep='\t', index=False)
    return reference
