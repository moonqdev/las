from typing import Tuple
import numpy as np
from scipy.spatial.transform import Rotation


def rotate_array_spherical(array: np.ndarray, rotation: Tuple[float, float, float]) -> np.ndarray:
    """Rotate array of shape (h, w, *) in assumption that array is represented as equirectangular panorama image

    Args:
        array (np.ndarray): array to rotate

        rotation (Tuple[float, float, float]): (roll, pitch, heading) Euler xyz angles in degrees

    Returns:
        np.ndarray: rotated array
    """
    # Array -> Sphere -> XYZ Matrix -> XYZ Vector -> Rotate -> Unwrap
    rotation = Rotation.from_euler('xyz', rotation, degrees=True)
    h, w, *_ = array.shape

    # create 1-unit sphere

    # horizontal angle
    phi = np.tile(np.flip(np.arange(w) / w) * 2 * np.pi, h)

    # vertical angle
    theta = (np.flip(np.arange(h) / h) * np.pi).repeat(w)

    # suppose that panorama is a sphere with r = 1
    r = np.tile(1, h * w)


    # transform from spherical to decart (xyz)
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)


    # from scipy.spatial.transform import Rotation
    # here we can use only [N, 3] array
    xyz = np.dstack((x, y, z)).squeeze()
    xyz_rotated = rotation.apply(xyz)


    x_rotated = xyz_rotated[:, 0]
    y_rotated = xyz_rotated[:, 1]
    z_rotated = xyz_rotated[:, 2]

    # Here we calculate planar distance to use in projection formulas
    # (projection from xyz to sphere for rotated points)
    # https://en.wikipedia.org/wiki/Spherical_coordinate_system ("Cartesian coordinates" part)

    # Here we have a lot of edge cases
    # For example process some zero values
    # This is done by numpy masks
    plane_d = np.sqrt(x_rotated ** 2 + y_rotated ** 2)

    theta_rotated = np.arctan(plane_d / z_rotated)
    theta_rotated[z_rotated < 0] += np.pi
    theta_rotated[((z_rotated == 0) * (x_rotated * y_rotated != 0))] = np.pi / 2

    phi_rotated = np.arctan(y_rotated / x_rotated)
    phi_rotated[((x_rotated < 0) * (y_rotated >= 0))] += np.pi
    phi_rotated[((x_rotated < 0) * (y_rotated < 0))] -= np.pi
    phi_rotated[((x_rotated == 0) * (y_rotated > 0))] = np.pi / 2
    phi_rotated[((x_rotated == 0) * (y_rotated < 0))] = np.pi / 2

    phi_rotated[phi_rotated < 0] += 2*np.pi
    phi_rotated[np.isnan(phi_rotated)] = 0

    pix_x_rotated = ((phi_rotated * w) / (2 * np.pi)).astype(np.int64)
    pix_y_rotated = ((theta_rotated * h) / (np.pi)).astype(np.int64)

    x_idx_rotated = np.flip(np.dstack(np.split(pix_x_rotated, h)).squeeze().T)
    y_idx_rotated = np.flip(np.dstack(np.split(pix_y_rotated, h)).squeeze().T)

    y_idx_rotated[y_idx_rotated >= h] = h - 1
    x_idx_rotated[x_idx_rotated >= w] = w - 1

    # OFFTOP
    # How would we write this in C
    # for i
    #   for j
    #       <rotate pixel>
    #       <calculate new place for this pixel>

    # Do permutation


    # Return array permutation
    # (initial y, x point become y, x somewhere else)
    # x/y_idx_rotated is reponsible for this permutation
    return array[y_idx_rotated, x_idx_rotated]