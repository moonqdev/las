import os
import shutil
import numpy as np
import pandas as pd
from tqdm import tqdm
from matplotlib import pyplot as plt

from converter.reference import map_reference
from converter.utils import npy_to_dat, dat_to_npy, pano_to_mtr
from converter.rotate import rotate_array_spherical
from converter.las import Center, LasDataset
from converter.angle import depth_to_xyz, get_normal_map, normal_map_to_image


ROLL = 'roll[deg]'
PITCH = 'pitch[deg]'
HEADING = 'heading[deg]'
X = 'projectedX[m]'
Y = 'projectedY[m]'
Z = 'projectedZ[m]'


DEFAULT_ELEVATION_LEVEL = 2.58


def save_buffer(buffer: np.ndarray, path: str, units = 10000):
    formatted_buffer = (buffer / units) * 100
    formatted_buffer = np.rint(formatted_buffer).astype(np.float64)

    # You better believe this code is necessary.
    # Otherwise, saved array will be rotated and doubled.
    # Don't ask why.
    h, w = buffer.shape
    arr = np.zeros((h, w))
    arr += formatted_buffer

    np.save(path, arr)
    return formatted_buffer


def process_dataset(panorama_reference_path: str, las_dir: str, las_mapping_path: str, pano_mapping_path: str, panorama_dir: str, save_dir: str):
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)

    reference_path = os.path.join(save_dir, "reference.csv")
    reference = map_reference(panorama_reference_path, reference_path)

    points = np.asarray(reference[[X, Y, Z]])
    rotations = np.asarray(reference[[ROLL, PITCH, HEADING]])

    centers = []
    for i in range(len(points)):
        xyz = points[i]
        rot = rotations[i]
        centers.append(Center(xyz, rot))

    pano_names = pd.read_csv(pano_mapping_path, sep=",", names=('name', 'smth', 'smth2'))['name']
    pano_paths = [os.path.join(panorama_dir, f"{name}.jpg") for name in pano_names]

    dataset = LasDataset(las_dir, las_mapping_path)

    pbar = tqdm(enumerate(centers))
    for i, center in pbar:
        # if i != 0:
            # continue

        pbar.set_description(f"Processing... {i + 1}/{len(centers)}")
        dir_name = os.path.join(save_dir, f'{i:012d}')
        if os.path.isdir(dir_name):
            shutil.rmtree(dir_name)

        os.mkdir(dir_name)

        panorama_path = os.path.join(dir_name, "panorama.jpg")
        mtr_path = os.path.join(dir_name, "fototiles.mtr")
        depth_mtr_path = os.path.join(dir_name, "depthtiles.mtr")
        npy_buffer_path = os.path.join(dir_name, "depth.npy")
        buffer_path = os.path.join(dir_name, "depth.dat")
        depth_path = os.path.join(dir_name, "depth.jpg")
        depthbm_path = os.path.join(dir_name, "depthbm.jpg")
        angle_path = os.path.join(dir_name, "angle.jpg")

        pbar.set_description(f"Creating buffer... {i + 1}/{len(centers)}")
        buffer, depth, depthbm = dataset.create_depthbuffer(center, 1024, 2048, 10000)
        if buffer is None:
            print("Buffer is None.")


        pbar.set_description(f"Correcting buffers... {i + 1}/{len(centers)}")
        panorama= plt.imread(pano_paths[i])
        panorama = rotate_array_spherical(panorama, 0 - center.rotation)
        buffer = rotate_array_spherical(buffer, 0 - center.rotation)
        depth = rotate_array_spherical(depth, 0 - center.rotation)
        depthbm = rotate_array_spherical(depthbm, 0 - center.rotation)

        pbar.set_description(f"Creating normal map... {i + 1}/{len(centers)}")
        normal_map = get_normal_map(buffer, 10000)
        normal_image = normal_map_to_image(normal_map)

        pbar.set_description(f"Saving results... {i + 1}/{len(centers)}")
        save_buffer(buffer, npy_buffer_path, 10000)
        plt.imsave(depth_path, depth, cmap="gray")
        plt.imsave(depthbm_path, depthbm, cmap="gray")
        plt.imsave(panorama_path, panorama)
        plt.imsave(angle_path, normal_image)
        pano_to_mtr(dir_name, "panorama.jpg", mtr_path)
        pano_to_mtr(dir_name, "depth.jpg", depth_mtr_path)
        npy_to_dat(npy_buffer_path, buffer_path)
