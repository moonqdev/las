from dataclasses import dataclass, field
from matplotlib import pyplot as plt
from PIL import ImageShow, Image
import numpy as np
import laspy as lp
import pandas as pd
import os
from scipy import signal
import pyproj
import math
import shutil
from scipy.spatial.transform import Rotation
import subprocess
import sys


from converter.interpolate import interpolate_depth


@dataclass
class Center:
    xyz: np.array
    rotation: np.array


class LasDataset:
    NEIGHBOURHOOD_SIZE = 75
    DEFAULT_ELEVATION_LEVEL = 2.58

    def __init__(self, las_dir_path: str, las_name_mapping_path: str):
        self.las_names = pd.read_csv(las_name_mapping_path, sep=',', names=('name', 'time'))['name']
        self.las_dir = las_dir_path

        self._create_las_frame(0)

    def _create_las_frame(self, idx):
        las_path = os.path.join(self.las_dir, f"{self.las_names[idx]}.las")
        self.frame = lp.read(las_path)
        self.crs = pyproj.CRS(self.frame.vlrs[0].string)

    def _is_projection_center_valid(self, center: Center):
        if (center.xyz < self.frame.header.mins).any():
            return False

        if (center.xyz > self.frame.header.maxs).any():
            return False

        return True

    def _get_srs_wkt(self):
        return self.frame.vlrs[0].string

    def _calibrate(self, center: Center):
        shifted_points = self.frame.xyz - center.xyz
        distances = np.sqrt(shifted_points[:, 0] ** 2 + shifted_points[:, 1] ** 2 + shifted_points[:, 2] ** 2)
        nearest_mask = distances < self.NEIGHBOURHOOD_SIZE
        shifted_points = shifted_points[nearest_mask]

        rotation = Rotation.from_euler('xyz', center.rotation, degrees=True)
        rotated_points = rotation.apply(shifted_points)

        x = rotated_points[:, 0]
        y = rotated_points[:, 1]
        z = rotated_points[:, 2]
        return x, y, z

    # def _crop_buffer(self, depthmap,  units=1000):
    def _crop_buffer(self, buffer,  units=1000):
        formatted_buffer = (buffer / units) * 100

        mask = formatted_buffer != 0
        formatted_buffer[mask] = 750 / formatted_buffer[mask]
        return formatted_buffer

    def _interpolate_map(self, depthmap, iterations=1):
        if iterations == 0:
            return depthmap

        result = depthmap.copy()
        kernel = np.array([
                        [0,1,0],
                        [1,0,1],
                        [0,1,0]
                        ])

        conv = signal.convolve2d(result, kernel, boundary='wrap', mode='same')/kernel.sum()
        mask = result==0
        result[mask] = conv[mask]
        return self._interpolate_map(result, iterations-1)

    def _map_postprocess(self, depthmap, units=1000, iterations=1):
        cropped_map = self._crop_buffer(depthmap)
        smoothed_map = self._interpolate_map(cropped_map, iterations)
        return smoothed_map

    def interpolate_buffer(self, buffer, units):
        d = buffer[:, :, 0]
        x = buffer[:, :, 1]
        y = buffer[:, :, 2]

        nonzero_mask = d != 0
        pts = np.dstack((x[nonzero_mask], y[nonzero_mask], d[nonzero_mask]))[0].transpose(1, 0) / units
        thin = interpolate_depth(pts, 2048, 1024, 2) * units
        thick = interpolate_depth(pts, 2048, 1024, 4) * units
        return thin, thick

    def get_elevation(self, buffer):
        bot = buffer[-5:]
        nonzero = (bot != 0)
        if not nonzero.any():
            return self.DEFAULT_ELEVATION_LEVEL
        return np.min(bot[nonzero])

    def recreate_ground(self, buffer, elevation=3.0, units=1000):
        h, w = buffer.shape
        plane = np.zeros((h, w))
        gradient = (np.pi / 2) * np.arange(h // 2, 0, -1) / (h // 2)
        ground = elevation * (1 / np.cos(gradient))
        ground[ground > self.NEIGHBOURHOOD_SIZE] = self.NEIGHBOURHOOD_SIZE
        ground = np.tile(ground, (w, 1)).T
        plane[h // 2:] = ground

        mask = (buffer == 0)
        buffer[mask] = plane[mask] * units

    def project_with_plane_info(self, point: Center, height, width, units=1000):
        if not self._is_projection_center_valid(point):
            return None, None

        xs, ys, zs = self._calibrate(point)
        rs = np.sqrt(np.square(xs) + np.square(ys) + np.square(zs))

        phis = np.arccos(zs / rs)
        thetas = np.arctan2(xs, ys)

        phi_indices = (phis / (np.pi)) * (height - 1)
        y_scaled = np.rint(np.clip(phi_indices, 0, height - 1) * units).astype(np.uint64)
        phi_indices = np.rint(phi_indices).astype(np.uint64)

        theta_indices = (thetas / (2*np.pi)) * (width) + width / 2
        x_scaled = np.rint(np.clip(theta_indices, 0, width - 1) * units).astype(np.uint64)
        theta_indices = np.rint(theta_indices).astype(np.uint64)
        theta_indices[theta_indices >= width] = width - 1
        theta_indices[theta_indices == width] = 0

        pix = np.rint(theta_indices * width + phi_indices).astype(np.uint64)
        dis = np.rint(rs * units).astype(np.uint64)

        couples = np.dstack((pix, dis, x_scaled, y_scaled))[0]
        couples = couples[couples[:, 0].argsort()]
        groups = np.split(couples, np.unique(couples[:, 0], return_index=True)[1])

        buffer = np.zeros((height, width, 3))
        for i, group in enumerate(groups):
            if len(group) == 0:
                continue
            pixel, distance, x_scaled, y_scaled = group[group[:, 1].argmin()]
            pixel_x = int(pixel // width)
            pixel_y = int(pixel % width)

            thriple = np.asarray([distance, x_scaled, y_scaled])
            buffer[pixel_y, pixel_x] += thriple

        thin, thick = self.interpolate_buffer(buffer, units)
        return thin, thick

    def create_depthbuffer(self, point: Center, height: int, width: int, units=1000):
        common_buffer = np.zeros((height, width))
        soft_buffer = np.zeros((height, width))
        for las_idx in range(len(self.las_names)):
            self._create_las_frame(las_idx)

            thin, thick = self.project_with_plane_info(point, height, width, units)
            if thin is None:
                continue
            if thick is None:
                continue

            mask = (common_buffer == 0)
            soft_mask = (soft_buffer == 0)
            common_buffer[mask] += thin[mask]
            soft_buffer[soft_mask] += thick[soft_mask]

        elevation = self.get_elevation(common_buffer) / units
        self.recreate_ground(common_buffer, elevation, units)
        self.recreate_ground(soft_buffer, elevation, units)

        depth = self._crop_buffer(common_buffer, units)
        depthm = self._crop_buffer(soft_buffer, units)
        return common_buffer, depth, depthm
