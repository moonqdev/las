import subprocess
import shutil
import os
import numpy as np


NPY_TO_DAT_EXECUTABLE_ARGS = ["maripoza", "transform-npy"]
DAT_TO_NPY_EXECUTABLE_ARGS = ["maripoza", "transform-dat"]
PANO_TO_MTR_EXECUTABLE_ARGS = ["maripoza", "pano-to-mtr"]


def npy_to_dat(npy_path: str, dat_path: str):
    npy = open(npy_path, 'r')
    dat = open(dat_path, 'w')
    p = subprocess.Popen(NPY_TO_DAT_EXECUTABLE_ARGS, stdin=npy, stdout=dat)
    p.wait()
    npy.close()
    dat.close()

def dat_to_npy(npy_path: str, dat_path: str):
    dat = open(dat_path, 'r')
    npy = open(npy_path, 'w')
    p = subprocess.Popen(DAT_TO_NPY_EXECUTABLE_ARGS, stdin=dat, stdout=npy)
    p.wait()
    dat.close()
    npy.close()

    # Script saves npy in f64 :(
    arr = np.load(npy_path)
    arr = arr.astype(np.float32)
    np.save(npy_path, arr)

def pano_to_mtr(pano_root: str, pano_name: str, mtr_path: str):
    args = PANO_TO_MTR_EXECUTABLE_ARGS + [pano_root, pano_name]
    p = subprocess.Popen(args)
    p.wait()

    result_mtr_path = os.path.join(pano_root, "0.mtr")
    shutil.move(result_mtr_path, mtr_path)
