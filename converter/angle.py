import numpy as np
from jakteristics import compute_features


def depth_to_xyz(depth):
    r = depth.copy()
    h, w = r.shape

    theta = (np.arange(h, 0, -1) / h) * np.pi
    theta = np.tile(theta, (w, 1)).T

    phi = (np.arange(w) / (w))* 2 * np.pi
    phi = np.tile(phi, (h, 1))

    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)

    xyz = np.dstack((x, y, z)).squeeze()
    return xyz

def get_normal_map(points, units=10000):
    formatted_points = (points / units) * 100
    formatted_points = np.rint(formatted_points).astype(np.float64)
    xyz = depth_to_xyz(formatted_points)

    h, w, _ = xyz.shape
    xyz = np.concatenate(xyz, axis=0).copy()
    normals = np.zeros(xyz.shape)
    x = xyz[:, 0]
    y = xyz[:, 1]
    z = xyz[:, 2]

    mask = ((x != 0) | (y != 0) | (z != 0))
    x = x[mask]
    y = y[mask]
    z = z[mask]
    xyz = np.dstack((x, y, z)).squeeze()

    feautres = compute_features(xyz, search_radius=10, feature_names=['nx', 'ny', 'nz'])
    normals[mask] = feautres
    normals_image_metric = np.dstack(np.split(normals, h)).transpose((2, 0, 1))
    normals_image_metric = np.nan_to_num(normals_image_metric)

    return normals_image_metric

def normal_map_to_image(normals):
    mask_x = normals[:, :, 0] != 0
    mask_y = normals[:, :, 1] != 0
    mask_z = normals[:, :, 2] != 0
    mask = mask_x | mask_y | mask_z

    normals_image = np.zeros(normals.shape, dtype=np.uint8)
    normals_image[mask] = np.uint8(255 * (normals[mask] * 0.5 + 0.5))
    return normals_image[:, :, [2, 0, 1]]